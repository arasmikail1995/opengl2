#ifndef CAMERA_H
#define CAMERA_H

// include files for QT
#include <QGLWidget>
#include <QKeyEvent>
#include <QtCore>

#include <windows.h>
#include <math.h>
#include <stdlib.h>

#include <GL/glu.h>



const float PI = 3.141592653589793;
class camera
{
public:
    camera();
    void RotateTranslatecamera(void);
    void keyPressEvent(QKeyEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    void Move();
private:
    bool keyUp, keyDown, keyLeft, keyRight,keyZ, keyQ, keyS, keyD, key8, key2;
    float xpos,ypos, zpos , xrot , yrot;
};

#endif // CAMERA_H
