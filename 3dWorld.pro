
TEMPLATE = app
TARGET = 3dWorld
DESTDIR = ./bin
QT += core gui opengl
QT += xml
CONFIG += debug console
DEFINES += QT_OPENGL_LIB QT_DLL
INCLUDEPATH += ./GeneratedFiles/Debug \
    ./include
LIBS += -lOpengl32 -lglu32
DEPENDPATH += .
MOC_DIR += ./GeneratedFiles/debug
OBJECTS_DIR += debug
UI_DIR += ./GeneratedFiles
RCC_DIR += ./GeneratedFiles

HEADERS += \
    camera.h \
    draw.h \
    studentElections.h \
    studentElectionsView.h

SOURCES += \
    main.cpp \
    camera.cpp \
    draw.cpp \
    studentElections.cpp \
    studentElectionsView.cpp

RESOURCES += \
    resources.qrc

OTHER_FILES += \
    3dWorld.pri




















