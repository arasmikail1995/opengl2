

#include "studentElections.h"

StudentElections::StudentElections()
{
    setWindowTitle("StudentElections");
    resize(1280, 720);
    m_view = new StudentElectionsView(this);
    setCentralWidget(m_view);
}

StudentElections::~StudentElections()
{
    delete m_view;
}
