#include "camera.h"

camera::camera()
{
}

void camera::RotateTranslatecamera(void) {
    glRotatef(xrot,1.0,0.0,0.0);  //rotate camera op de x as
    glRotatef(yrot,0.0,1.0,0.0);  //rotate camera op de y as
    glTranslated(-xpos,-ypos,-zpos); //translate
}

void camera::keyPressEvent(QKeyEvent *e)
{
        if(e->key() == Qt::Key_Down)
        {
            keyDown = true;
        }
        if(e->key() == Qt::Key_Up)
        {
            keyUp =true;
        }
        if(e->key() == Qt::Key_Z)
        {
            keyZ = true;
        }
        if(e->key() == Qt::Key_S)
        {
            keyS = true;
        }
        if(e->key() == Qt::Key_Right)
        {
            keyRight = true;
        }
        if(e->key() == Qt::Key_Left)
        {
            keyLeft = true;
        }
        if(e->key() == Qt::Key_Q)
        {
            keyQ = true;
        }
        if(e->key() == Qt::Key_D)
        {
            keyD = true;
        }

        if(e->key() == Qt::Key_8)
        {
            key8 = true;
        }
        if(e->key() == Qt::Key_2)
        {
            key2 = true;
        }
}
void camera::keyReleaseEvent(QKeyEvent *e)
{
    if(e->key() == Qt::Key_Down)
    {
        keyDown = false;
    }
    if(e->key() == Qt::Key_Up)
    {
        keyUp =false;
    }
    if(e->key() == Qt::Key_Z)
    {
        keyZ = false;
    }
    if(e->key() == Qt::Key_S)
    {
        keyS = false;
    }
    if(e->key() == Qt::Key_Right)
    {
        keyRight = false;
    }
    if(e->key() == Qt::Key_Left)
    {
        keyLeft = false;
    }
    if(e->key() == Qt::Key_Q)
    {
        keyQ = false;
    }
    if(e->key() == Qt::Key_D)
    {
        keyD = false;
    }

    if(e->key() == Qt::Key_8)
    {
        key8 = false;
    }
    if(e->key() == Qt::Key_2)
    {
        key2 = false;
    }

}

void camera::Move()
{
    float xrotrad, yrotrad;
    if (keyUp)
    {
        xrot -= 1;
            if (xrot < -360)
                xrot += 360;
    }
    else if (keyDown)
    {
        xrot += 1;
        if (xrot >360)
            xrot -= 360;
    }
    if (keyLeft)
    {
        yrot -= 3;
        if (yrot < -360)
            yrot += 360;
    }
    else if (keyRight)
    {
        yrot += 3;
        if (yrot >360)
            yrot -= 360;
    }
    if(keyZ)
    {
        yrotrad = (yrot / 180 * PI);
        xrotrad = (xrot / 180 * PI);
        xpos += float(sin(yrotrad)) * 0.5;
        zpos -= float(cos(yrotrad)) * 0.5;
        ypos -= float(sin(xrotrad)) * 0.5;
    }
    else if(keyS)
    {
        yrotrad = (yrot / 180 * PI);
        xrotrad = (xrot / 180 * PI);
        xpos -= float(sin(yrotrad)) * 0.5;
        zpos += float(cos(yrotrad)) * 0.5;
        ypos += float(sin(xrotrad)) * 0.5;
    }
    if(keyD)
    {
        yrotrad = (yrot / 180 * PI);
        xpos += float(cos(yrotrad)) * 0.3;
        zpos += float(sin(yrotrad)) * 0.3;
    }
    else if(keyQ)
    {
        yrotrad = (yrot / 180 * PI);
        xpos -= float(cos(yrotrad)) * 0.3;
        zpos -= float(sin(yrotrad)) * 0.3;
    }
    if(key8)
        ypos+= 0.3;
    else if(key2)
        ypos-= 0.3;
}
