#ifndef STUDENTELECTIONS_H
#define STUDENTELECTIONS_H

#include <windows.h>
#include <QMainWindow>

#include "studentElectionsView.h"

#include <QGLWidget>
#include <QKeyEvent>
#include <QtCore>

#include <math.h>
#include <stdlib.h>

#include <GL/glu.h>


class StudentElections : public QMainWindow
{
	Q_OBJECT

public:
    StudentElections();
    ~StudentElections();
private:
    StudentElectionsView *m_view;
};
#endif 

