#include "draw.h"

Draw::Draw()
{
    spotlight = true;
}

int Draw::initializeObjects()
{
    QDomDocument document;
    QFile file(":/world.xml");

    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Failed to open file";
        return -1;
    }
    else
    {
        if(!document.setContent(&file))
        {
            qDebug() << "Failed to load document";
            return -1;
        }
        file.close();
    }

    //root
    QDomElement root = document.firstChildElement();


    QDomNodeList list = root.childNodes();

    for(int i = 0; i < list.count(); i++)
    {
        /*textures van objecten setten*/
        if(list.at(i).nodeName().compare("Textures") == 0)
        {
            QDomNodeList textures = list.at(i).childNodes();

            for(int j = 0; j < textures.count(); j++)
            {
                if(textures.at(j).isElement())
                {
                    QString type;
                    QDomElement elem = textures.at(j).toElement();
                    type = elem.attribute("type");

                    if(type.compare("lineair") == 0)
                        texture[j] = LoadTexture(elem.text(),LINEAIR);
                    else if(type.compare("mipmap") == 0)
                        texture[j] = LoadTexture(elem.text(),MIPMAP);
                    else if(type.compare("repeat") == 0)
                        texture[j] = LoadTexture(elem.text(),REPEAT);
                    else /*default, bij foute ingave van type -> nearest*/
                        texture[j] = LoadTexture(elem.text(),NEAREST);
                }
            }
        }
        /*skybox setten*/
        else if(list.at(i).nodeName().compare("Skybox") == 0)
        {
            QDomNodeList skybox = list.at(i).childNodes();
            for(int j = 0; j < skybox.count(); j++)
            {
                if(skybox.at(j).isElement())
                {
                    QString type;
                    QDomElement elem = skybox.at(j).toElement();
                    type = elem.attribute("type");

                    if(type.compare("lineair") == 0)
                        sky[j] = LoadTexture(elem.text(),LINEAIR);
                    else if(type.compare("mipmap") == 0)
                        sky[j] = LoadTexture(elem.text(),MIPMAP);
                    else if(type.compare("repeat") == 0)
                        sky[j] = LoadTexture(elem.text(),REPEAT);
                    else /*default, bij foute ingave van type -> nearest*/
                        sky[j] = LoadTexture(elem.text(),NEAREST);
                }
            }
        }
        /*display lists maken*/
        else if(list.at(i).nodeName().compare("Model") == 0)
        {
            QDomNodeList models = list.at(i).childNodes();
            for(int j = 0; j < models.count(); j++)
            {
                if(models.at(j).nodeName().compare("Ground") == 0)
                    createGroundDL(models.at(j));
                else if(models.at(j).nodeName().compare("House") == 0)
                    createHouseDL(models.at(j));
                else if(models.at(j).nodeName().compare("Chair") == 0)
                    createChairDL(models.at(j));
                else if(models.at(j).nodeName().compare("Table") == 0)
                    createTableDL(models.at(j));
                else if(models.at(j).nodeName().compare("Stage") == 0)
                    createStageDL(models.at(j));
            }
            createDoorDL();
            createlightSwitchDL();
            createIntroDL();
            createTreeDL();
            createClockDL();
        }
    }
}

int Draw::SetScene()
{
    QDomDocument document;
    QFile file(":/world.xml");

    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Failed to open file";
        return -1;
    }
    else
    {
        if(!document.setContent(&file))
        {
            qDebug() << "Failed to load document";
            return -1;
        }
        file.close();
    }

    //root
    QDomElement root = document.firstChildElement();


    QDomNodeList list = root.childNodes();

    for(int i = 0; i < list.count(); i++)
    {
        if(list.at(i).nodeName().compare("Scene") == 0)
        {
            drawObjects(list.at(i));
        }
    }
    if(!light)
        glEnable(GL_LIGHT0);
    else
        glDisable(GL_LIGHT0);
    glDisable(GL_LIGHT2);
    drawSky(50,25,50,100,50,100);
}

void Draw::createGroundDL(QDomNode root)
{
    QDomNodeList list = root.childNodes();
    groundDL = glGenLists(1);

    glNewList(groundDL,GL_COMPILE);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texture[0]);


    for(int i = 0; i < list.count(); i++)
    {
        if(list.at(i).nodeName().compare("Quads") == 0)
        {
            makeMiniQuads(list.at(i));
        }
    }
    glDisable(GL_TEXTURE_2D);

    glEndList();
}

void Draw::createStageDL(QDomNode root){
    QDomNodeList list = root.childNodes();
    stageDL = glGenLists(1);

    glNewList(stageDL,GL_COMPILE);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texture[4]);

    for(int i = 0; i < list.count(); i++)
    {
        QDomNodeList list2 = list.at(i).childNodes();
        for(int j = 0; j < list2.count(); j++)
        {
            if(list2.at(j).nodeName().compare("Quads") == 0)
                makeMiniQuads(list2.at(j));
        }
    }
    glDisable(GL_TEXTURE_2D);
    glEndList();
}

void Draw::createHouseDL(QDomNode root)
{
    QDomNodeList list = root.childNodes();

    /*buitenkant van huis aparte dp omdat er 2 lichten zijn, 1 licht voor de hele wereld (dus ook buitenkant van het huis) en 1 licht voor enkel de binnenkant van het huis*/
    houseDL[0] = glGenLists(1);

    glNewList(houseDL[0],GL_COMPILE);
    glEnable(GL_TEXTURE_2D);

    for(int i = 0; i < list.count(); i++)
    {
        QDomNodeList list2 = list.at(i).childNodes();
        for(int j = 0; j < list2.count(); j++)
        {
            QString wall = list.at(i).nodeName();
            /*muren aan de buitenkant*/
            if(wall.compare("WallLeft") == 0 || wall.compare("WallRight") == 0 ||
                    wall.compare("WallAchter") == 0 || wall.compare("WallVoor") == 0
                    || wall.compare("Plafond") == 0)
            {
                glBindTexture(GL_TEXTURE_2D,texture[1]);
                if(list2.at(j).nodeName().compare("Quads") == 0)
                    makeMiniQuads(list2.at(j));
            }

        }
    }
    glDisable(GL_TEXTURE_2D);
    glEndList();

    houseDL[1] = glGenLists(1);

    glNewList(houseDL[1],GL_COMPILE);
    glEnable(GL_TEXTURE_2D);
    for(int i = 0; i < list.count(); i++)
    {
        QDomNodeList list2 = list.at(i).childNodes();
        for(int j = 0; j < list2.count(); j++)
        {
            QString wall = list.at(i).nodeName();

            if(wall.compare("Floor") == 0)
            {
                glBindTexture(GL_TEXTURE_2D,texture[2]);
            }
            else /*muren aan de binnenkant*/
            {
                glBindTexture(GL_TEXTURE_2D,texture[1]);
            }

            /*build the house*/
            if(list2.at(j).nodeName().compare("Quads") == 0)
                makeMiniQuads(list2.at(j));
        }
    }
    glEndList();
}

void Draw::createChairDL(QDomNode root)
{
    float coordinate[3];
    GLfloat amb[] = {0.135f,0.2225f,0.1575f,0.95};
    GLfloat diff[] = {0.54f,0.89f,0.63f,0.95};
    GLfloat specular[] = {0.316228f,0.316228f,0.316228f,0.95f};
    GLfloat shininess[] = {12.8};

    QDomNodeList list = root.childNodes();

    chairDL = glGenLists(1);

    glNewList(chairDL,GL_COMPILE);

    glMaterialfv(GL_FRONT,GL_SPECULAR,specular);
    glMaterialfv(GL_FRONT,GL_SHININESS,shininess);
    glMaterialfv(GL_FRONT,GL_AMBIENT,amb);
    glMaterialfv(GL_FRONT,GL_DIFFUSE,diff);

    glColor3f(0.1f,0,0);
    for(int i = 0; i < list.count(); i++)
    {
        glBegin(GL_QUAD_STRIP);
        QDomNodeList list2 = list.at(i).childNodes();
        /*telkens de vertex lezen en builden*/
        for(int j = 0; j < list2.count(); j++)
        {
            QDomNodeList coordinates = list2.at(j).childNodes();
            for(int k = 0; k < coordinates.count(); k++)
            {
                if(coordinates.at(k).isElement())
                {
                    QDomElement elem = coordinates.at(k).toElement();
                    coordinate[k] = elem.text().toFloat();
                }
            }
            glVertex3f(coordinate[0],coordinate[1],coordinate[2]);
         }
        glEnd();
    }
    glEndList();
}

void Draw::createTableDL(QDomNode root)
{
    float coordinate[3];
    GLfloat amb[] = {0.135f,0.2225f,0.1575f,0.95};
    GLfloat diff[] = {0.54f,0.89f,0.63f,0.95};
    GLfloat specular[] = {0.316228f,0.316228f,0.316228f,0.95f};
    GLfloat shininess[] = {12.8};
    QDomNodeList list = root.childNodes();

    tableDL = glGenLists(1);

    glNewList(tableDL,GL_COMPILE);

    glMaterialfv(GL_FRONT,GL_SPECULAR,specular);
    glMaterialfv(GL_FRONT,GL_SHININESS,shininess);
    glMaterialfv(GL_FRONT,GL_AMBIENT,amb);
    glMaterialfv(GL_FRONT,GL_DIFFUSE,diff);

    glColor3f(0.1f,0.0f,0.0f);

    for(int i = 0; i < list.count(); i++)
    {
        if(list.at(i).nodeName().compare("Leg") == 0)
        {
            /*4 poten*/
            for(int l = 0; l < 4; l++)
            {
            glBegin(GL_QUAD_STRIP);
            QDomNodeList list2 = list.at(i).childNodes();
            for(int j = 0; j < list2.count(); j++)
            {
                /*telkens de vertex lezen en builden*/
                QDomNodeList coordinates = list2.at(j).childNodes();
                for(int k = 0; k < coordinates.count(); k++)
                {
                    if(coordinates.at(k).isElement())
                    {
                       QDomElement elem = coordinates.at(k).toElement();
                       coordinate[k] = elem.text().toFloat();
                    }
                }
                glVertex3f(coordinate[0],coordinate[1],coordinate[2]);
            }
            glEnd();
            /*de positie van elke poot setten*/
            if(l == 0)
                glTranslatef(0.0f, 0.0f, 1.0f);
            else if(l == 1)
                glTranslatef(2.0f, 0.0f, 0.0f);
            else if(l == 2)
                glTranslatef(0.0f, 0.0f, -1.0f);
            else if(l == 3)
                glTranslatef(-2.0f, 0.0f, 0.0f);
            }

        }
        /*bovenkant van de tafel builden*/
        else if(list.at(i).nodeName().compare("Bovenkant") == 0)
        {
            glBegin(GL_QUADS);
            QDomNodeList list2 = list.at(i).childNodes();
            for(int j = 0; j < list2.count(); j++)
            {
                QDomNodeList coordinates = list2.at(j).childNodes();
                for(int k = 0; k < coordinates.count(); k++)
                {
                    if(coordinates.at(k).isElement())
                    {
                       QDomElement elem = coordinates.at(k).toElement();
                       coordinate[k] = elem.text().toFloat();
                    }
                }
                glVertex3f(coordinate[0],coordinate[1],coordinate[2]);
            }
             glEnd();
        }
    }
    glEndList();
}

void Draw::createlightSwitchDL()
{
    lightSwitchDL = glGenLists(1);

    glNewList(lightSwitchDL,GL_COMPILE);

    glColor3f(1.0f,1.0f,1.0f);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texture[5]);

    glBegin(GL_QUADS);
    glTexCoord2d(0,0);
    glVertex3f(0,0,0);
    glTexCoord2d(1,0);
    glVertex3f(0,0,1);
    glTexCoord2d(1,1);
    glVertex3f(0,1,1);
    glTexCoord2d(0,1);
    glVertex3f(0,1,0);
    glNormal3f(0,0,-1);
    glEnd();

    glEndList();
}

void Draw::createDoorDL()
{
    doorDL = glGenLists(1);

    glNewList(doorDL,GL_COMPILE);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texture[6]);

    glBegin(GL_QUADS);
    glTexCoord2d(0,0);
    glVertex3f(0,0,0);
    glTexCoord2d(1,0);
    glVertex3f(5,0,0);
    glTexCoord2d(1,1);
    glVertex3f(5,10,0);
    glTexCoord2d(0,1);
    glVertex3f(0,10,0);
    glNormal3f(0,0,-1);
    glEnd();

    glEndList();
}

/*build het paadje voor het huis*/
void Draw::createIntroDL()
{
    introDL = glGenLists(1);
    glNewList(introDL,GL_COMPILE);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texture[7]);

    glBegin(GL_QUADS);
    glTexCoord2d(0,0);
    glVertex3f(0,0,0);
    glTexCoord2d(10,0);
    glVertex3f(2,0,0);
    glTexCoord2d(10,10);
    glVertex3f(2,0,23);
    glTexCoord2d(0,10);
    glVertex3f(0,0,23);
    glEnd();

    glBegin(GL_QUADS);
    glTexCoord2d(0,0);
    glVertex3f(0,0,6);
    glTexCoord2d(10,0);
    glVertex3f(25,0,6);
    glTexCoord2d(10,10);
    glVertex3f(25,0,8);
    glTexCoord2d(0,10);
    glVertex3f(0,0,8);
    glEnd();

    glEndList();
}

void Draw::createTreeDL()
{
    treeDL = glGenLists(1);
    glNewList(treeDL,GL_COMPILE);
    /*stam van de boom*/
    glColor3f(0.5f, 0.35f, 0.05f);
    glBegin(GL_QUAD_STRIP);
    glVertex3f(0,0,0);
    glVertex3f(0,5,0);
    glVertex3f(0,0,0.5);
    glVertex3f(0,5,0.5);
    glVertex3f(0.5,0,0.5);
    glVertex3f(0.5,5,0.5);
    glVertex3f(0.5,0,0);
    glVertex3f(0.5,5,0);
    glVertex3f(0,0,0);
    glVertex3f(0,5,0);
    glNormal3f(-1,0,0);
    glEnd();

    /*blaren(bol) van de boom*/
    glTranslatef(0.25,5,0.25);

    glColor3f(0,0.5,0);
    glBegin(GL_LINE_LOOP);
    GLUquadricObj* quadric = gluNewQuadric();

    gluQuadricDrawStyle(quadric, GLU_FILL);
    gluSphere(quadric, 1, 25, 25);

    gluDeleteQuadric(quadric);
    glEnd();

    glColor3f(1,1,1);// reset color
    glEndList();
}

void Draw::createClockDL()
{
    clockDL[0] = glGenLists(1);
    glNewList(clockDL[0],GL_COMPILE);

    glBegin(GL_TRIANGLE_FAN);
    glColor3f(0.0,0.2,1.0);
    glVertex3f(0,0,0);
    for(int angle = 0; angle <= 360; angle++)
    {
        glVertex3f(cos(angle * PIE/180.0f),sin(angle * PIE/180.0f), 0);
    }
    glEnd();
    glEndList();

    /*seconde wijzer*/
    clockDL[1] = glGenLists(1);
    glNewList(clockDL[1],GL_COMPILE);
    glColor3f(0.0,0.0,0.0);
    glBegin(GL_QUADS);
    glVertex3d(0,0,0.01);
    glVertex3d(0.4,0,0.01);
    glVertex3d(0.4,0.01,0.01);
    glVertex3d(0,0.01,0.01);
    glEnd();
    glEndList();

    /*minute wijzer*/
    clockDL[2] = glGenLists(1);
    glNewList(clockDL[2],GL_COMPILE);
    glColor3f(0.0,0.0,0.0);
    glBegin(GL_QUADS);
    glVertex3d(0,0,0.01);
    glVertex3d(0.4,0,0.01);
    glVertex3d(0.4,0.02,0.01);
    glVertex3d(0,0.02,0.01);
    glEnd();
    glEndList();

    /*uur wijzer*/
    clockDL[3] = glGenLists(1);
    glNewList(clockDL[3],GL_COMPILE);
    glColor3f(0.0,0.0,0.0);
    glBegin(GL_QUADS);
    glVertex3d(0,0,0.01);
    glVertex3d(0.2,0,0.01);
    glVertex3d(0.2,0.02,0.01);
    glVertex3d(0,0.02,0.01);
    glEnd();
    glEndList();
}

void Draw::drawObjects(QDomNode root)
{
    float coordinate[3];
    float angle;
    QDomNodeList list = root.childNodes();
    QTime time = QTime::currentTime();

    /*klok*/
    seconds = secondMinuteAngleCalculator(time.second());
    minutes = secondMinuteAngleCalculator(time.minute());
    hours = hourAngleCalculator(time.hour());


    for(int i = 0; i < list.count(); i++)
    {
        QDomNodeList numberofobjects = list.at(i).childNodes();
        for(int j = 0; j < numberofobjects.count(); j++)
        {
            QDomNodeList coordinates = numberofobjects.at(j).childNodes();
            // bewaar de huidige modelview matrix voordat men een object tekent
            glMatrixMode( GL_MODELVIEW );
            glPushMatrix( );

            for(int k = 0; k < coordinates.count(); k++)
            {
                QDomNodeList coord = coordinates.at(k).childNodes();
                for(int l = 0; l < coord.count(); l++)
                {
                    if(coord.at(l).isElement())
                    {
                        QDomElement elem = coord.at(l).toElement();
                        coordinate[l] = elem.text().toFloat();
                    }
                }
                /*coordinaten van de positie van het object bewaren*/
                if(coordinates.at(k).nodeName().compare("coord") == 0)
                {
                    glTranslatef(coordinate[0],coordinate[1],coordinate[2]);
                }
                /*scale van het object bewaren*/
                else if(coordinates.at(k).nodeName().compare("scale") == 0)
                {
                    glScalef(coordinate[0],coordinate[1],coordinate[2]);
                }
                /*rotatie bewaren*/
                else if(coordinates.at(k).nodeName().compare("rotate") == 0)
                {
                    if(coordinates.at(k).isElement())
                    {
                        QDomElement elem = coordinates.at(k).toElement();
                        angle = elem.attribute("angle").toFloat();
                    }
                    glRotatef(angle,coordinate[0],coordinate[1],coordinate[2]);

                    /*de graden van de wijzers van de klok refreshen*/
                    if(numberofobjects.at(j).nodeName().compare("SecondeWijzer") == 0)
                    {
                        glPushName(CLOCK);
                        glRotated( (GLdouble)(seconds), 0.0, 0.0, 1.0 );
                        glCallList(clockDL[1]);
                        glPopName();
                    }
                    else if(numberofobjects.at(j).nodeName().compare("MinuteWijzer") == 0)
                    {
                        glPushName(CLOCK);
                        glRotated( (GLdouble)(minutes), 0.0, 0.0, 1.0 );
                        glCallList(clockDL[2]);
                        glPopName();
                    }
                    else if(numberofobjects.at(j).nodeName().compare("UurWijzer") == 0)
                    {
                        glPushName(CLOCK);
                        glRotated( (GLdouble)(hours), 0.0, 0.0, 1.0 );
                        glCallList(clockDL[3]);
                        glPopName();
                    }
                }
            }
            /*objecten tekenen door de bijhorende display list op te roepen*/
            if(list.at(i).nodeName().compare("Ground") == 0)
            {
                if(!light)
                    glEnable(GL_LIGHT0);
                else
                    glDisable(GL_LIGHT0);
                glDisable(GL_LIGHT2);
                glPushName(GRASS);
                glCallList(groundDL);
                glPopName();
            }
            else if(list.at(i).nodeName().compare("House") == 0)
            {
                glPushName(HOUSE);

                if(!light)
                    glEnable(GL_LIGHT0);
                else
                    glDisable(GL_LIGHT0);
                glDisable(GL_LIGHT2);
                glCallList(houseDL[0]);

                if(!roomlight)
                    glEnable(GL_LIGHT2);
                else
                    glDisable(GL_LIGHT2);
                glDisable(GL_LIGHT0);

                glCallList(houseDL[1]);

                glPopName();
            }
            else if(list.at(i).nodeName().compare("Chair") == 0)
            {
                if(!roomlight)
                    glEnable(GL_LIGHT2);
                else
                    glDisable(GL_LIGHT2);
                glDisable(GL_LIGHT0);
                glPushName(CHAIR);
                glCallList(chairDL);
                glPopName();
            }
            else if(list.at(i).nodeName().compare("Table") == 0)
            {
                if(!roomlight)
                    glEnable(GL_LIGHT2);
                else
                    glDisable(GL_LIGHT2);
                glDisable(GL_LIGHT0);
                glPushName(TABLE);
                glCallList(tableDL);
                glPopName();
            }
            else if(list.at(i).nodeName().compare("LightSwitch") == 0)
            {
                if(!roomlight)
                    glEnable(GL_LIGHT2);
                else
                    glDisable(GL_LIGHT2);
                glDisable(GL_LIGHT0);
                glPushName(LIGHTSWITCH);
                glCallList(lightSwitchDL);
                glPopName();
            }
            else if(list.at(i).nodeName().compare("Door") == 0)
            {
                if(!roomlight)
                    glEnable(GL_LIGHT2);
                else
                    glDisable(GL_LIGHT2);
                glDisable(GL_LIGHT0);
                glPushName(DOOR);
                glCallList(doorDL);
                glPopName();
            }
            else if(list.at(i).nodeName().compare("Stage") == 0)
            {
                if(!light)
                    glEnable(GL_LIGHT0);
                else
                    glDisable(GL_LIGHT0);
                glDisable(GL_LIGHT2);
                glPushName(STAGE);
                glCallList(stageDL);
                glPopName();
            }
            else if(list.at(i).nodeName().compare("Tree") == 0)
            {
                if(!light)
                    glEnable(GL_LIGHT0);
                else
                    glDisable(GL_LIGHT0);
                glDisable(GL_LIGHT2);
                glPushName(TREE);
                glCallList(treeDL);
                glPopName();
            }
            else if(list.at(i).nodeName().compare("Intro") == 0)
            {
                if(!light)
                    glEnable(GL_LIGHT0);
                else
                    glDisable(GL_LIGHT0);
                glDisable(GL_LIGHT2);
                glPushName(GROUND);
                glCallList(introDL);
                glPopName();
            }
            else if(list.at(i).nodeName().compare("Clock") == 0)
            {
                if(!roomlight)
                    glEnable(GL_LIGHT2);
                else
                    glDisable(GL_LIGHT2);
                glDisable(GL_LIGHT0);
                glPushName(CLOCK);
                glCallList(clockDL[0]);
                glPopName();
            }
            // restore de huidige modelview matrix nadat men een object heeft getekend
            glMatrixMode( GL_MODELVIEW );
            glPopMatrix( );
        }
    }
}
/*berekent de graden van de wijzer voor het uur*/
double Draw::hourAngleCalculator(int hour)
{
    double angle;
    hour = hour % 12;
    angle = hour * 30.0;
    return -angle;
}
/*berekent de graden van de wijzer van de seconden en de minuten*/
double Draw::secondMinuteAngleCalculator(int minuteSecond)
{
    double angle;
    angle = minuteSecond * 6.0;
    return -angle;
}


void Draw::drawSky(float x, float y, float z, float width, float height, float length)
{
            x = x - width  / 2;
            y = y - height / 2;
            z = z - length / 2;

            /* Front side*/
            glBindTexture(GL_TEXTURE_2D, sky[0]);
            glBegin(GL_QUADS);
                    glTexCoord2f(1.0f, 0.0f);
                    glVertex3f(x,y,z+length);
                    glTexCoord2f(1.0f, 1.0f);
                    glVertex3f(x,y+height,z+length);
                    glTexCoord2f(0.0f, 1.0f);
                    glVertex3f(x+width,y+height,z+length);
                    glTexCoord2f(0.0f, 0.0f);
                    glVertex3f(x+width,y,z+length);
            glEnd();

            /*Back side*/
            glBindTexture(GL_TEXTURE_2D, sky[1]);
            glBegin(GL_QUADS);
                    glTexCoord2f(1.0f, 0.0f);
                    glVertex3f(x+width,y,z);
                    glTexCoord2f(1.0f, 1.0f);
                    glVertex3f(x+width,y+height,z);
                    glTexCoord2f(0.0f, 1.0f);
                    glVertex3f(x,y+height,z);
                    glTexCoord2f(0.0f, 0.0f);
                    glVertex3f(x,y,z);
            glEnd();

            /*Left side*/
            glBindTexture(GL_TEXTURE_2D, sky[2]);
            glBegin(GL_QUADS);
                    glTexCoord2f(1.0f, 1.0f);
                    glVertex3f(x,y+height,z);
                    glTexCoord2f(0.0f, 1.0f);
                    glVertex3f(x,y+height,z+length);
                    glTexCoord2f(0.0f, 0.0f);
                    glVertex3f(x,y,z+length);
                    glTexCoord2f(1.0f, 0.0f);
                    glVertex3f(x,y,z);
            glEnd();

            /*Right side*/
            glBindTexture(GL_TEXTURE_2D, sky[2]);
            glBegin(GL_QUADS);
                    glTexCoord2f(0.0f, 0.0f);
                    glVertex3f(x+width,y,z);
                    glTexCoord2f(1.0f, 0.0f);
                    glVertex3f(x+width,y,z+length);
                    glTexCoord2f(1.0f, 1.0f);
                    glVertex3f(x+width,y+height,z+length);
                    glTexCoord2f(0.0f, 1.0f);
                    glVertex3f(x+width,y+height,z);
            glEnd();

            /*Up side*/
            glBindTexture(GL_TEXTURE_2D, sky[3]);
            glBegin(GL_QUADS);
                    glTexCoord2f(0.0f, 0.0f);
                    glVertex3f(x+width,y+height,z);
                    glTexCoord2f(1.0f, 0.0f);
                    glVertex3f(x+width,y+height,z+length);
                    glTexCoord2f(1.0f, 1.0f);
                    glVertex3f(x,y+height,z+length);
                    glTexCoord2f(0.0f, 1.0f);
                    glVertex3f(x,y+height,z);
            glEnd();
}

/*maak van een grote vierkant, kleine vierkantjes*/
void Draw::makeMiniQuads(QDomNode node)
{
    float coordinate[3];
    QString doesntChange;//coordinaat dat niet verandert bij de quads

    QDomNodeList coordinates = node.childNodes();
    for(int j = 0; j < coordinates.count(); j++)
    {
        if(coordinates.at(j).isElement())
        {
            QDomElement elem = coordinates.at(j).toElement();
            coordinate[j] = elem.text().toFloat();
        }
    }
    float x,y,z;
    x = coordinate[0];
    y = coordinate[1];
    z = coordinate[2];

    if(node.isElement())
    {
    QDomElement elemnt = node.toElement();
    doesntChange = elemnt.attribute("doesntChange");
    }


    if(doesntChange.compare("x") == 0)
    {
        for(int i = 0; i < y; i++)
        {
            for(int j = 0; j < z; j++)
            {
                glBegin(GL_QUADS);
                glTexCoord2d(0,0);
                glVertex3f(x,i,j);
                glTexCoord2d(1,0);
                glVertex3f(x,i,j+1);
                glTexCoord2d(1,1);
                glVertex3f(x,i+1,j+1);
                glTexCoord2d(0,1);
                glVertex3f(x,i+1,j);
                glNormal3f(0,0,-1);
                glEnd();
            }
        }
    }
    else if(doesntChange.compare("y") == 0)
    {
        for(int i = 0; i < x; i++)
        {
            for(int j = 0; j < z; j++)
            {
                glBegin(GL_QUADS);
                glTexCoord2d(0,0);
                glVertex3f(i,y,j);
                glTexCoord2d(1,0);
                glVertex3f(i,y,j+1);
                glTexCoord2d(1,1);
                glVertex3f(i+1,y,j+1);
                glTexCoord2d(0,1);
                glVertex3f(i+1,y,j);
                glNormal3f(0,0,-1);
                glEnd();
            }
        }
    }
    else if(doesntChange.compare("z") == 0)
    {
        for(int i = 0; i < x; i++)
        {
            for(int j = 0; j < y; j++)
            {
                glBegin(GL_QUADS);
                glTexCoord2d(0,0);
                glVertex3f(i,j,z);
                glTexCoord2d(1,0);
                glVertex3f(i+1,j,z);
                glTexCoord2d(1,1);
                glVertex3f(i+1,j+1,z);
                glTexCoord2d(0,1);
                glVertex3f(i,j+1,z);
                glNormal3f(0,0,-1);
                glEnd();
            }
        }
    }
}

GLuint Draw::LoadTexture(QString filename,Textures textType)
{
    /*textures*/
    GLuint text;
    QImage img = QImage(filename);
    if(img.isNull())
    {
        qDebug() << "Failed to open image " << filename;
        return -1;
    }
    img = QGLWidget::convertToGLFormat(img);
    glGenTextures(1, &text);
    glBindTexture(GL_TEXTURE_2D, text);

    if(textType == NEAREST)
    {
    /*nearest*/
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, 3, img.width(), img.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, img.bits());
    }
    else if(textType == REPEAT)
    {
    /*repeat*/
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexImage2D(GL_TEXTURE_2D, 0, 3, img.width(), img.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, img.bits());
    }

    else if(textType == LINEAIR)
    {
    /*linear*/
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, 3, img.width(), img.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, img.bits());
    }
    else if(textType == MIPMAP)
    {
    /*mipmap*/
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
        gluBuild2DMipmaps(GL_TEXTURE_2D, 3, img.width(), img.height(), GL_RGBA, GL_UNSIGNED_BYTE, img.bits());
    }
    return text;
}
