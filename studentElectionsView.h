#ifndef STUDENTELECTIONSVIEW_H
#define STUDENTELECTIONSVIEW_H
#include "camera.h"
#include "draw.h"
#include <QMouseEvent>
#include <QTimer>

#include <QGLWidget>
#include <QKeyEvent>
#include <QtCore>

#include <math.h>
#include <stdlib.h>

#include <GL/glu.h>

enum Visualizations {FLAT,SMOOTH,WIREFRAME};

/*voor picking*/
enum Mode {SELECT,RENDER};

const int BUFFER = 512;

class StudentElectionsView : public QGLWidget
{
	Q_OBJECT
public:
    StudentElectionsView(QWidget *parent=0);
    ~StudentElectionsView();

private:
	void initializeGL ();
	void resizeGL ( int width, int height );
	void paintGL ();

    void keyPressEvent(QKeyEvent * e);
    void keyReleaseEvent(QKeyEvent *e) {cam.keyReleaseEvent(e);}
    void mousePressEvent(QMouseEvent *e);

    int cursorX,cursorY;
	double camPosx,camPosy,camPosz;
	double camUpx,camUpy,camUpz;
    double camViewx,camViewy,camViewz;
    QTimer* timer;
    camera cam;

    //klasse voor objecten

    Draw draw;
    Visualizations visual;

    //picking
    Mode mode;
    GLuint selectBuf[BUFFER];
    GLint hits;
    GLint viewport[4];
    void startPicking();
    void stopPicking();
    void processHits(GLint hits,GLuint buffer[]);
};

#endif
