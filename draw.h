#ifndef DRAW_H
#define DRAW_H

#include <windows.h>
#include <QGLWidget>
#include <QtXml>
#include <QtCore>
#include <QTime>

#include <GL/glu.h>


enum objects{GRASS,GROUND,HOUSE,CHAIR,TABLE,BOARD,LIGHTSWITCH,DOOR,TREE,WINDOW,CLOCK,STAGE};
enum Textures {NEAREST = 0,LINEAIR = 1,MIPMAP = 2,REPEAT = 3};

const float PIE = 3.141592653589793;
class Draw
{
public:
    Draw();

    /*leest textures van xml, leest objecten van xml en maakt displaylists hiervan*/
    int initializeObjects();

    /*roept de display lists op door de objecten uit de xml te lezen*/
    int SetScene();

    bool GetLight() const{return light;}
    bool GetSpotLight() const{return spotlight;}
    bool GetRoomLight() const{return roomlight;}

    void SetLight(bool lightt) {light=lightt;}
    void SetSpotLight(bool light) {spotlight = light;}
    void SetRoomLight(bool light) {roomlight = light;}
private:
    /*laadt een texture met gegeven type en geeft het terug*/
    GLuint LoadTexture(QString filename,Textures textType);

    /*create display lists*/
    void createGroundDL(QDomNode root);
    void createHouseDL(QDomNode root);
    void createChairDL(QDomNode root);
    void createTableDL(QDomNode root);
    void createStageDL(QDomNode root);
    void createlightSwitchDL();
    void createDoorDL();
    void createIntroDL();
    void createTreeDL();
    void createClockDL();

    /*set objects in world (call lists)*/
    void drawObjects(QDomNode root);
    void drawSky(float x, float y, float z, float width, float height, float length);

    /*maakt kleine quads van een grote quads, wordt gebruikt bij huis en grond*/
    void makeMiniQuads(QDomNode node);

    /*berekent de hoek van de overeenkomstige uur/minuut/seconde wijzer*/
    double hourAngleCalculator(int hour);
    double secondMinuteAngleCalculator(int minuteSecond);

    GLuint texture[8],sky[4];
    GLuint groundDL,houseDL[2],chairDL,tableDL,lightSwitchDL,doorDL,introDL,treeDL,stageDL,clockDL[4];
    bool light,spotlight,roomlight;
    objects object;
    double seconds,minutes,hours;
};

#endif // DRAW_H
