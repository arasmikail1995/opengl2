#include "studentElectionsView.h"

StudentElectionsView::StudentElectionsView(QWidget *parent) : QGLWidget(parent)
{
    timer = new QTimer();
    connect( timer, SIGNAL(timeout()), this, SLOT(updateGL()) );

    setFocusPolicy(Qt::StrongFocus);

    camPosx = 30.0,  camPosy = 1.8,    camPosz = 30.0;
    camViewx = 0.0, camViewy = 0.5, camViewz = 0.0;
    camUpx = 0.0,   camUpy = 1.0,   camUpz = 0.0;

    mode = RENDER;
    visual = SMOOTH;
}

StudentElectionsView::~StudentElectionsView()
{
    delete timer;
}

void StudentElectionsView::initializeGL ()
{
	// Initialize QGLWidget (parent)
	QGLWidget::initializeGL();

	// Black canvas
    glClearColor(0.0f,0.0f,0.0f,0.0f);


    /*light 0 (globaal licht)*/
    GLfloat ambientLight[]={0.1,0.1,0.1,1.0};
    glLightfv(GL_LIGHT0,GL_AMBIENT,ambientLight);

    GLfloat diffuseLight[]={0.8,0.8,0.8,1.0};
    glLightfv(GL_LIGHT0,GL_DIFFUSE,diffuseLight);

    GLfloat specularLight[]={0.5,0.5,0.5,1.0};
    glLightfv(GL_LIGHT0,GL_SPECULAR,specularLight);

    GLfloat lightPos[]={0.0,30.0,60.0,0.0};
    glLightfv(GL_LIGHT0,GL_POSITION,lightPos);

    GLfloat specularReflection[]={1.0,1.0,1.0,1.0};
    glMaterialfv(GL_FRONT, GL_SPECULAR, specularReflection);
    glMateriali(GL_FRONT,GL_SHININESS,128);

    /*light 2 (kamerlicht)*/
    GLfloat ambientLight2[]={1.0,1.0,1.0,1.0};
    glLightfv(GL_LIGHT2,GL_AMBIENT,ambientLight2);

    glLightfv(GL_LIGHT2,GL_DIFFUSE,diffuseLight);

    glLightfv(GL_LIGHT2,GL_SPECULAR,specularLight);

    GLfloat lightPos2[]={20.0,3.0,20.0,0.0};
    glLightfv(GL_LIGHT2,GL_POSITION,lightPos2);

    glMaterialfv(GL_FRONT, GL_SPECULAR, specularReflection);
    glMateriali(GL_FRONT,GL_SHININESS,128);


    /*spotlight*/
    GLfloat ambientSpotLight[]={0.05,0.05,0.05,1.0};
    glLightfv(GL_LIGHT1,GL_AMBIENT,ambientSpotLight);

    GLfloat diffuseSpotLight[]={0.8,0.8,0.8,1.0};
    glLightfv(GL_LIGHT1,GL_DIFFUSE,diffuseSpotLight);

    GLfloat SpotlightPos[]={ 0.0,0.0,0.0,1.0};
    glLightfv(GL_LIGHT1,GL_POSITION,SpotlightPos);

    GLfloat spotDir[]={0.0,0.0,-1.0};
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spotDir);
    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF,6.0);
    glLightf(GL_LIGHT1, GL_SPOT_EXPONENT,10.0);

    // Place light
    glEnable( GL_LIGHTING );
    glEnable( GL_LIGHT0 );
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);

    timer->start(50);

    /*read objects from xml, create display lists ,set textures and set skybox*/
    draw.initializeObjects();
}

void StudentElectionsView::resizeGL ( int width, int height )
{
	if ((width<=0) || (height<=0))
		return;

	//set viewport
	glViewport(0,0,width,height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//set persepective
	//change the next line order to have a different perspective
	GLdouble aspect_ratio=(GLdouble)width/(GLdouble)height;
    gluPerspective(45.0f, aspect_ratio, 1.0,200.0);

	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void StudentElectionsView::paintGL ()
{
    cam.Move();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();

    cam.RotateTranslatecamera();

    if(mode == SELECT)
        startPicking();

    gluLookAt(camPosx ,camPosy ,camPosz,
              camViewx,camViewy,camViewz,
              camUpx, camUpy, camUpz );

	// store current matrix
    glMatrixMode( GL_MODELVIEW );
    glPushMatrix( );

    /*lichten*/
    GLfloat light0_position [] = {30.0, 10.0, 30.0, 0};
    GLfloat light_diffuse []={ 1.0, 1.0, 1.0, 1.0 };
    glLightfv ( GL_LIGHT0, GL_POSITION, light0_position );
    glLightfv ( GL_LIGHT0, GL_AMBIENT, light_diffuse );

    GLfloat light1_position[] = { -0.5, 0.5, -1.0, 1.0 };
    glLightfv ( GL_LIGHT0, GL_POSITION, light1_position );

    /*spotlight aan en uit zetten*/
    if(draw.GetSpotLight())
        glDisable(GL_LIGHT1);
    else
        glEnable(GL_LIGHT1);

    qglColor(Qt::white);

    /*set visualisation
      key I = smooth
      key O = flat
      ket P = wireframe*/

    if(visual == SMOOTH)
        glShadeModel(GL_SMOOTH);
    else if(visual == FLAT)
        glShadeModel(GL_FLAT);
    else if(visual == WIREFRAME)
    {
        glPolygonMode(GL_FRONT, GL_LINE);
        glPolygonMode(GL_BACK, GL_LINE);
    }

    /*draw the objects*/
    draw.SetScene();

    /*end wireframe*/
    if(visual == WIREFRAME)
    {
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_FILL);
    }

    if(mode == SELECT)
        stopPicking();

    // restore current matrix
    glMatrixMode( GL_MODELVIEW );
    glPopMatrix( );


}

void StudentElectionsView::startPicking()
{
    GLint viewport[4];
    float ratio;

    glGetIntegerv (GL_VIEWPORT, viewport);

    glSelectBuffer (BUFFER, selectBuf);
    glRenderMode (GL_SELECT);

    glInitNames();

    glMatrixMode (GL_PROJECTION);
    glPushMatrix ();
    glLoadIdentity ();

    gluPickMatrix ((GLdouble) cursorX, (GLdouble) (viewport[3] - cursorY),
                      5.0, 5.0, viewport);


    ratio = (viewport[2]+0.0) / viewport[3];
    gluPerspective(45,ratio,0.1,1000);
    glMatrixMode(GL_MODELVIEW);
}

void StudentElectionsView::stopPicking() {

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glFlush();
    hits = glRenderMode(GL_RENDER);
    if (hits != 0){
        processHits(hits,selectBuf);
    }
    mode = RENDER;
}

void StudentElectionsView::processHits(GLint hits,GLuint buffer[])
{
    GLint i, numberOfNames;
    GLuint names, *ptr, minZ,*ptrNames;

    ptr = (GLuint *) buffer;
    minZ = 0xffffffff;
    for (i = 0; i < hits; i++) {
        names = *ptr;
        ptr++;
        if (*ptr < minZ) {
            numberOfNames = names;
            minZ = *ptr;
            ptrNames = ptr+2;
        }

        ptr += names+2;
    }
    if (numberOfNames > 0) {
        ptr = ptrNames;
        switch(*ptr)
        {
            case GROUND:
                qDebug() << "That's the ground";
            break;
            case GRASS:
                qDebug() << "That's the grass";
            break;
            case HOUSE:
                qDebug() << "That's a house";
                break;
            case CHAIR:
                qDebug() << "That's a chair";
                break;
            case TABLE:
                qDebug() << "That's a table";
                break;
            case BOARD:
                qDebug() << "That's a board";
            break;
            case DOOR:
                qDebug() << "That's a door";
            break;
            case WINDOW:
                qDebug() << "That's a window";
            break;
            case TREE:
                qDebug() << "That's a tree";
            break;
            case CLOCK:
                qDebug() << "That's a clock";
            break;
            case STAGE:
                qDebug() << "That's the stage";
            break;
            case LIGHTSWITCH:
                if(!draw.GetRoomLight())
                    draw.SetRoomLight(true);
                else
                    draw.SetRoomLight(false);
            break;
        }
   }
}


void StudentElectionsView::keyPressEvent(QKeyEvent * e)
{
    if(e->key() == Qt::Key_I)
    {
        visual = SMOOTH;
    }
    else if(e->key() == Qt::Key_O)
    {
        visual = FLAT;
    }
    else if(e->key() == Qt::Key_P)
    {
        visual = WIREFRAME;
    }
    else if(e->key() == Qt::Key_L)
    {
        if(!draw.GetLight())
            draw.SetLight(true);
        else
            draw.SetLight(false);
    }
    else if(e->key() == Qt::Key_M)
    {
        if(!draw.GetSpotLight())
            draw.SetSpotLight(true);
        else
            draw.SetSpotLight(false);
    }
    else
        cam.keyPressEvent(e);
}


void StudentElectionsView::mousePressEvent(QMouseEvent *e)
{
    if(e->button() == Qt::LeftButton)
    {
        mode = SELECT;
        cursorX = e->x();
        cursorY = e->y();
    }
}
